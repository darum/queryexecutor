﻿#include "sqlexecutor.h"
#include "commons.h"

#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>

#include <QVariant>

SqlExecutor::SqlExecutor(QString & driver, QString & server, int port
            ,QString & database, QString & user, QString & pass)
    :mColumns(NULL), mDataList(NULL), mLastSql(""), addFlag(false)
{
    mQSql = QSqlDatabase::addDatabase(driver);
    mQSql.setHostName(server);
    mQSql.setPort(port);
    mQSql.setDatabaseName(database);
    mQSql.setUserName(user);
    mQSql.setPassword(pass);
}

const QString SqlExecutor::getConnectInfo()
{
    QString connectInfo;
    QString driver = mQSql.driverName();
    connectInfo += driver;

    if (driver == "QODBC") {
        connectInfo += "; DSN=";
        connectInfo += mQSql.databaseName();
        connectInfo += "; user=";
        connectInfo += mQSql.userName();
    }

    return connectInfo;
}

/** SQL実行
 * @brief SqlExecutor::executeQuery
 * @param sql SQL
 * @return true:成功, false:失敗
 */
bool SqlExecutor::executeQuery(QString &sql)
{
    bool ret = true;

    addFlag = false;     // 追加表示かどうか
    if (sql == mLastSql) {
        if (mDataList->size() == 1) {
            addFlag = true;
        }
    }

    if (ret) {
        // DB接続
        ret = this->mQSql.open();
        if (!ret) {
            this->mLastErrMsg = mQSql.lastError().text();
            return ret;
        }

    }

    QSqlQuery query(mQSql);
    if (ret) {
        // Prepare
        ret = query.prepare(sql);
        if (!ret) {
            this->mLastErrMsg = query.lastError().text();
        }
    }

#if 0   // TODO 判定できないのでふた
    if (ret) {
        // SELECTかどうかの事前判定
        ret = query.isSelect();
        if(!ret) {
            this->mLastErrMsg = S("SELECT文以外は実行できません");
        }
    }
#endif

    if (ret) {
        // Query
        ret = query.exec();
        if (!ret) {
            this->mLastErrMsg = query.lastError().text();
        } else {
            // SQLを保管しておく
            this->mLastSql = query.lastQuery();
        }
    }

    if (ret) {
        // データの保管
        // Get ColumnList
        QSqlRecord record = query.record();

        if (!addFlag) {
            // データクリア
            if (this->mColumns != NULL) {
                delete [] this->mColumns;
            }
            if (this->mDataList != NULL) {
                delete this->mDataList;
            }

            mColumnCount = record.count();
            this->mColumns = new QString[mColumnCount];     // DBColumn Name (Array index is position of columns)

            for (int idx = 0; idx < record.count(); ++idx) {
                QString name = record.fieldName(idx);
                mColumns[record.indexOf(name)] = name;
            }

            this->mDataList = new QList< QMap<int, QString> >();
        }

        // Fetch結果の取得
        while(query.next()) {
            // 1DBRows
            QMap<int, QString> fetchData;   // Fetchしたデータ(1行)

            for (int queryPos = 0; queryPos < mColumnCount; queryPos++) {
                fetchData[queryPos] = query.value(queryPos).toString();
            }

            this->mDataList->append(fetchData);
        }
    }

    // DB閉じる
    mQSql.close();

    return ret;
}

QString SqlExecutor::lastErrMsg()
{
    return this->mLastErrMsg;
}

void SqlExecutor::getColumns(QString **columns, int &columnCount)
{
    *columns = mColumns;
    columnCount = this->mColumnCount;
}

QList< QMap<int, QString> > SqlExecutor::dataList()
{
    return *this->mDataList;
}

bool SqlExecutor::isAdd()
{
    return addFlag;
}
