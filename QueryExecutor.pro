#-------------------------------------------------
#
# Project created by QtCreator 2015-02-13T01:30:16
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QueryExecutor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sqlexecutor.cpp

HEADERS  += mainwindow.h \
    sqlexecutor.h \
    commons.h

FORMS    += mainwindow.ui

VERSION = 0.1.0.0
