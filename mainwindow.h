#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QSettings>

#include "sqlexecutor.h"

// バージョン定義
const int VER_MAJOR = 0;
const int VER_MINOR = 2;
const int VER_DEV = 0;
const int VER_BUILD = 0;    // 未使用

namespace Ui {
class MainWindow;
}

#define CONFIG_FILE "DBConfig.ini"
#define DEFAULT_COL_WIDTH 100

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void displayData(QString* columns, int columnCount, QList< QMap<int, QString> > &dispDataList, bool diff);

private:
    QString version();

private:
    Ui::MainWindow *ui;
    SqlExecutor *sqlExec;

public slots:
    void queryExecute();
    void clearTable();
    void autoSize(bool);
};

#endif // MAINWINDOW_H
