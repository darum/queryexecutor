﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sqlexecutor.h"
#include "commons.h"

#include <QDebug>

#include <QMessageBox>
#include <QTableWidget>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>

#include <QList>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // WIndowCaption
    this->setWindowTitle(tr("QueryExecutor: Ver ") + version());

    ui->sqlText->setFocus();

    // Ini読み込み
    QSettings config(CONFIG_FILE, QSettings::IniFormat);
    qDebug() << config.fileName();
    config.beginGroup(tr("DB"));
    QString driver = config.value(tr("DRIVER"), QString("")).toString();
    QString server = config.value("SERVER", QString("")).toString();
    int port = config.value("PORT", 0).toInt();
    QString database = config.value("DATABASE", QString("")).toString();
    QString user = config.value("USER", QString("")).toString();
    QString pass = config.value("PASS", QString("")).toString();

    bool isValid = true;
    bool isSupported = true;    // TODO サポートドライバで判定
    if (driver == "") {
        isValid = false;
    } else if (driver == "QODBC"){
        if ((database == "") || (user == "") || (pass == "")) {
            isValid = false;
        }
    } else {
        isSupported = false;
    }

    if (!isValid) {
        QMessageBox msgBox(this);
        msgBox.setText(S("iniファイルエラー"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        exit(1);
    }

    if (!isSupported) {
        QMessageBox msgBox(this);
        msgBox.setText(S("サポートしていません：[") + driver + S("]"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        exit(1);
    }

    this->sqlExec = new SqlExecutor(driver, server, port, database, user, pass);

    this->ui->connectInfoLabel->setText(S("接続情報：") + this->sqlExec->getConnectInfo());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::queryExecute() {
    // 入力SQL文
    QString sql = this->ui->sqlText->toPlainText();

    // Message出力
    QTextBrowser* msg = ui->msgArea;

    QMessageBox msgBox(this);
    if (sql.length() == 0) {
        msgBox.setText(QString::fromLocal8Bit("SQLが入力されていません"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        return;
    }

    qDebug("[%s]", qPrintable(sql));
    msg->append(S("DB Process Start."));
    if (!sqlExec->executeQuery(sql)) {
        msg->append(sqlExec->lastErrMsg());
        return;
    }
    msg->append(S("DB Process Finish."));

    QString *columns;
    int columnCount;
    sqlExec->getColumns(&columns, columnCount);
    QList< QMap<int, QString> > dispDataList = sqlExec->dataList();

    // Mapping to Table
    this->displayData(columns, columnCount, dispDataList, sqlExec->isAdd());

    ui->autiSizeCheck->setChecked(false);
}

void MainWindow::clearTable() {
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(0);
    ui->tableWidget->setRowCount(0);
}

/**
 * @brief MainWindow::displayData
 * @param columns
 * @param columnCount
 * @param dispDataList
 * @param dispDiff 比較表示を行うか(true=1列目と比較し変化を表示）
 */
void MainWindow::displayData(QString* columns, int columnCount, QList< QMap<int, QString> > &dispDataList, bool dispDiff)
{
    QTableWidget* tbl = ui->tableWidget;
    int dataCount = dispDataList.size();
    // Tableの初期化
    tbl->setRowCount(columnCount);
    tbl->setColumnCount(dataCount);

    if (dataCount == 0) {
        ui->msgArea->append(S("件数は0件です"));
    }
    else {
        for (int row = 0; row < columnCount; ++row) {
            // DBColumn
            tbl->setVerticalHeaderItem(row, new QTableWidgetItem(columns[row]));

            // Display datas
            QMap<int, QString> *baseData = &dispDataList[0];
            for (int col = 0; col < dataCount; ++col) {
                QMap<int, QString> *data = &dispDataList[col];
                QTableWidgetItem* cell = new QTableWidgetItem(data->value(row));
                if (dispDiff) {
                    if (col != 0) {
                        if (baseData->value(row) != data->value(row)) {
                            // 異なっている場合には色を塗る
                            cell->setBackgroundColor(Qt::yellow);
                        }
                    }
                }
                tbl->setItem(row, col, cell);
            }
        }
    }
}

void MainWindow::autoSize(bool check)
{
    if (check) {
        // 自動調整
        ui->tableWidget->resizeColumnsToContents();
    }
    else
    {
        // 固定幅
        for (int col = 0; col < ui->tableWidget->columnCount(); ++col) {
            ui->tableWidget->setColumnWidth(col, DEFAULT_COL_WIDTH);
        }
    }
}

QString MainWindow::version()
{
    QString str;
    str.sprintf("%d.%d.%d", VER_MAJOR, VER_MINOR, VER_DEV);
    return str;
}
