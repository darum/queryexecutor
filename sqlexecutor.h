﻿#ifndef SQLEXECUTOR_H
#define SQLEXECUTOR_H

#include <QString>
#include <QList>
#include <QMap>

#include <QtSql/QSqlDatabase>
#include <QSqlError>

class SqlExecutor
{
public:
    SqlExecutor(QString & driver, QString & server, int port
                ,QString & database, QString & user, QString & pass);

    QString lastErrMsg();
    void getColumns(QString **columns, int & columnCount);
    QList< QMap<int, QString> > dataList();

    bool executeQuery(QString & sql);

    const QString getConnectInfo();

    bool isAdd();

private:
    // DataBase
    QSqlDatabase mQSql;
    // Select結果のカラム名リスト（配列番号=カラムの順序）
    QString* mColumns;
    int mColumnCount;
    // Select結果のデータ（キー=カラム番号、値=データ）
    QList< QMap<int, QString> > *mDataList;

    QString mLastSql;
    QString mLastErrMsg;

    bool addFlag;
};

#endif // SQLEXECUTOR_H
